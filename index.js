const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    port = 5000,
    mongoPort = 27017;
    mongo = require('./mongo.js');

mongo(mongoPort, Schema => {
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.use(bodyParser.json());

    app.get('/', (req, res) => 
        Schema.find().then(list =>
            res.json(list.map(doc =>
                doc.data))));

    app.post('/', (req, res) => 
        Schema.create({data: req.body.data}) & res.json());
    
    app.listen(port, () => {
        console.log(`Aplication is runnimg at http://localhost:${port}`);
    }); 
});