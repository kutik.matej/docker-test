var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var blogSchema = new Schema({
    data: String
});

module.exports = (port, callback) => {
    mongoose.connect(`mongodb://mongo:${port}/test`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .catch(error => console.error(error))
    .then(() => {
        console.log("Database connected successfully");
        
        callback(mongoose.model('Blog', blogSchema));
    });
}